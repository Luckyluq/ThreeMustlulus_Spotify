from SPARQLWrapper import SPARQLWrapper
import rdflib
from django.shortcuts import render
from django.http import HttpResponse
from homepage.rdf_utils import look_up

def index(request):
    response = {}
    return render(request, 'homepage.html', response)

def search_song(request):
    if request.method == "POST":
        result = look_up(request.POST["query"])
        print(result)
        return render(request, 'homepage.html', result)
