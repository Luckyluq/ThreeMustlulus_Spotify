# Query from local

import rdflib
from SPARQLWrapper import SPARQLWrapper
import re

def urifier(word, rdf_type):
    split = word.split(" ")
    for i in range(len(split)):
        split[i] = split[i].capitalize()
    res =  "_".join(split)
    return rdf_type + ':' + res

def song_urifier(word, case):
  word = word.replace("-", "") #remove -
  word = word.replace("'", "") #remove '
  if case == "insensitive":
    split = word.split(" ")
    for i in range(len(split)):
        split[i] = split[i].capitalize()
    res =  "_".join(split)
  else: #sensitive
    split = word.split(" ")
    res =  "_".join(split)

  # print(res)
  return res

def uri_remover(word):
  temp = word.split("resource/",1)[1]
  res = temp.replace("_", " ")
  return res

def local_query(song_input, case):
    g = rdflib.Graph()
    result = g.parse("data.ttl", format="n3")
    # print("Querying data from local .ttl") #Testing
    # print("Graph has %s statements." % len(g))

    data = []

    song_queried = song_urifier(song_input, case)
    # print(song_queried) #testing

    que = """
        PREFIX ex: <http://example.org/top_spotify/>
        PREFIX dbp: <http://dbpedia.org/property/>
        PREFIX dbo: <http://dbpedia.org/ontology/>
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

        SELECT DISTINCT ?label ?artist ?url
        WHERE {{
            ?song rdfs:label ?label ;
                dbo:artist ?artist ;
                dbp:url ?url .
            FILTER regex(str(?song), "%s") .
        }}  
        """% song_queried

    queres = g.query(que)
    result = dictify(queres)

    return result
    

def dictify(data):
    dict = {"songLabel" : [], "artist" : [], "url" : []}
    
    for i in data:
        song_cut = i[0][0:].replace(" (song)","")
        dict["songLabel"].append(song_cut)
        dict["artist"].append(i[1][0:])
        dict["url"].append(i[2][0:])
    
    return dict

# Query from DBPedia

def dbpedia_query(artist_input):
    artist_queried = urifier(artist_input, 'dbr')
    # print(artist_queried)
    # print("Querying data from dbpedia.org") #Testing

    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery("""
        PREFIX dbo: <http://dbpedia.org/ontology/>
        PREFIX dbr: <http://dbpedia.org/resource/>
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>


        SELECT DISTINCT ?name ?abstract ?genreLabel
        WHERE {{
        {{
            {0} a dbo:MusicalArtist ;
            foaf:name ?name ;
            dbo:abstract ?abstract .
        }} 
        UNION
        {{
            {0} a dbo:Band ;
            foaf:name ?name ;
            dbo:abstract ?abstract .
        }} 
        UNION
        {{
            {0} a dbo:Singer ;
            foaf:name ?name ;
            dbo:abstract ?abstract .
        }} 
        UNION
        {{
            {0} a dbo:Person ;
            foaf:name ?name ;
            dbo:abstract ?abstract .
        }}
            FILTER(lang(?abstract) = 'en') .
            OPTIONAL {{ {0} dbo:genre ?genre . 
            ?genre rdfs:label ?genreLabel
            FILTER(lang(?genreLabel) = 'en') .}} .
        }}
    """.format(artist_queried))

    # print(sparql) #testing
    sparql.setReturnFormat('json')
    results = sparql.query().convert()

    # print(results) #testing

    try:
        name = results["results"]["bindings"][0]["name"]["value"]
        abstract = results["results"]["bindings"][0]["abstract"]["value"]
        genreLabel = results["results"]["bindings"][0]["genreLabel"]["value"]
        return {'name' : name, 'abstract' : abstract, 'genreLabel' : genreLabel} #Kalo ada genre
    except:
        try:
            return {'name' : name, 'abstract' : abstract} #Kalo TIDAK ada genre
        except:
            return {}  #Return kosong/nothing krn NO SINGER FOUND

# Integration

def look_up(song_title):
    resDict = {"song": [], "artist" : [], "url" : [], "abstract" : [], "genre" : []}
    # res = ""
    artist_list = []
    abstract_list = []
    genre_list = []

    local = local_query(song_title,"insensitive") #insert desired song substring to look for here
    ###Testing
    # print ("LOCAL (case insensitive):\n" + str(local) + "\n")
    ###
    artist = local["artist"][0:] #ambil list artistURI
    print(str(artist) + "\n") #Testing
    
    if len(artist) == 0: #check if ada artis -> if gaada: lokal ga nemu lagunya
        local = local_query(song_title,"sensitive")
        ###Testing
        # print ("LOCAL (case sensitive):\n" + str(local) + "\n") 
        ###
        artist = local["artist"][0:] #ambil list artistURI
        print(str(artist) + "\n") #Testing
        if len(artist) == 0:
            return ("Song not Found")
        #   ###Testing
        # else:
        #   print("Artist URI:\n " + str(artist) + "\n") 
        #   ###

    for artist_uri in artist: #ambil setiap artis, lalu jalankan dbpedia_query
        # res = res + (str(artist_uri) + "\n") #Testing
        artist_nonUri = uri_remover(artist_uri) #dapet artis
        artist_list.append(artist_nonUri) #nanti dimasukkin ke resDict
        # print(artist_nonUri) #Testing
        DBP_res = dbpedia_query(artist_nonUri)

        try:
            name = DBP_res["name"]

            abstract = DBP_res["abstract"].replace("'","")
            abstract_list.append(abstract)

            # res = res + (">Name: " + name + "\n")
            # res = res + (">Abstract: " + abstract + "\n")
            try:
                genre = DBP_res["genreLabel"]
                # res = res + (">Genre: " + genre)
                genre_list.append(genre)
            except:
                # res = res + (">Genre: -")
                genre_list.append("-")
        except:
            # res = res + ("Singer not Found in DB / Complicated Artist URI")
            abstract_list.append("Not Found")
            genre_list.append("Not Found")

    for i in range(len(local["songLabel"])): #gaharus songLabel, cuma biar tau panjangnya aja
        resDict["song"].append(local["songLabel"][i].replace("'",""))
        resDict["url"].append(local["url"][i])
        resDict["artist"].append(artist_list[i])
        resDict["abstract"].append(abstract_list[i])
        resDict["genre"].append(genre_list[i])

    return resDict

# Testing
def testing():
    song_input = "goosebumps"
    out = look_up(song_input)
    
    # Test
    if out != "Song not Found":
        header = ["Song: ", "Spotify URL: ", "Artist: ", "Abstract: ", "Genre: "]
        keysOut = ["song", "url", "artist", "abstract", "genre"]
        for i in range(len(out["song"])):
            for j in range(len(out.keys())):
                print(header[j] + str(out[keysOut[j]][i]))
                print("\n")
    else:
        print("Song not Found")